//
//  SpaceAdventure.swift
//  SpaceAdventure
//
//  Created by GDD Student on 18/4/16.
//  Copyright © 2016 Your School. All rights reserved.
//

import Foundation
import GameplayKit

class SpaceAdventure{
    
    let planetarySystem: PlanetarySystem
    
    
    init(wow: PlanetarySystem){
        planetarySystem = wow
    }
                  
    
    
    
    private func displayIntroduction()
    {
        let diameterOfEarth = 24859.82 // In miles, from pole to pole.
        
        //Print introduction
        print("Welcome to our \(planetarySystem.name)!")
        print("There are \(planetarySystem.planets.count) planets to explore.")
        print("Your are currently on Earth, which has a circumference of \(diameterOfEarth) miles.")
    }
    
    private func greetAdventurer()
    {
        //Print user
        let name = responseToPrompt("What is your name ?")
        print("Nice to meet you, \(name). My name is CORTANA, I am a friend of Siri")
    }
    
    private func responseToPrompt(prompt: String)->String{
        print(prompt)
        return getln()
    }
    
    private func determineDestination()
    {
        var decision = ""
        while (decision != "Y" && decision != "N"){
            
            decision = responseToPrompt("Shall I randomly choose a planet for you to visit (Y or N)")
            
            if decision == "Y"{
                
                if let planet = planetarySystem.randomPlanet {
                    visit(planet.name)
                
                } else {
                    print("I'm sorry, this planetary has no planets to visit.")
                }
                
                
            } else if (decision == "N"){
                let planetName = responseToPrompt("Ok, name the planet you would like to visit...")
                visit(planetName)
            } else {
                print("Sorry, I didn't get that")
            }
        }
    }
    
    private func visit(planetName: String){
        print("Travelling to \(planetName)...")
        for var i = 0; i < planetarySystem.planets.count; i++
        {
            let planet = planetarySystem.planets[i]
            if planetName == planet.name{
                print("Arrived at \(planetName). The \(planet.description)")
            }
        }
    }
    
    func start(){
        
        displayIntroduction()
        greetAdventurer()
        if (!planetarySystem.planets.isEmpty){
            print("Let's go on an adventure!")
            determineDestination()
            print("Starting your adventure!")
        }

    }
    
}