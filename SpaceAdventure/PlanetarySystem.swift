//
//  PlanetarySystem.swift
//  SpaceAdventure
//
//  Created by GDD Student on 21/4/16.
//  Copyright © 2016 Your School. All rights reserved.
//

import GameKit
import Foundation

class PlanetarySystem{
    let name:String
    
    var planets: [Planet]
    
    init(name: String, planets: [Planet]){
        self.name = name
        self.planets = planets
    }
    
    var randomPlanet:Planet? {
        if planets.isEmpty {
            return nil
        } else {
            
            let chosen = GKRandomSource.sharedRandom().nextIntWithUpperBound(planets.count)
            // OR
            // let rand = GKRandomSource.sharedRandom()
            // let chosen = rand.nextIntWithUpperBound(planets.count)
            
            return planets[chosen]
        }
        
    }
    
}

class Planet {
    let name: String
    let description: String
    
    init (name: String, description: String){
        self.name = name
        self.description = description
    }
}

