/*

This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike
4.0 International License, by Yong Bakos.

*/

import Foundation

let systemName = "Solar System"

let planetData = [

    "Mercury": "first planet from the sun.",
    "Venus": "second planet from the sun.",
    "Earth": "third planet from the sun.",
    "Mars": "fourth planet from the sun.",
    "Jupiter": "fifth planet from the sun.",
    "Uranus": "seventh planet from the sun.",
    "Neptune": "eighth planet from the sun.",
    "Pluto": "ninth planet from the sun."

]


var planets = planetData.map { name, description in
    Planet(name: name, description: description)
}


let x = PlanetarySystem(name: systemName, planets: planets)

let adventure = SpaceAdventure(wow: x)
adventure.start()